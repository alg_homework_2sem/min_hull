/*
 * Дан неориентированный связный граф. Требуется найти вес минимального
 * остовного дерева в этом графе. Алгоритм Прима. Ассимптотика - O(ElogV), O(V)
 * - память
 */
#include <iostream>
#include <set>
#include <tuple>
#include <vector>

using std::pair;
using std::set;
using std::vector;
struct Edge {
  int from, to, cost;
  bool operator<(const Edge &a) {
    return std::tie(cost, from, to) < std::tie(a.cost, a.from, a.to);
  }
};
long long getMinSpanningTreeCost(const vector<vector<Edge>> &graph);

int main() {
  int verticesCount, edgeCount;
  std::cin >> verticesCount >> edgeCount;
  vector<vector<Edge>> graph(verticesCount);
  for (int i = 0; i < edgeCount; i++) {
    int from, to, weight;
    std::cin >> from >> to >> weight;
    graph[from - 1].push_back({from - 1, to - 1, weight});
    graph[to - 1].push_back({to - 1, from - 1, weight});
  }
  std::cout << getMinSpanningTreeCost(graph) << std::endl;
  return 0;
}

long long getMinSpanningTreeCost(const vector<vector<Edge>> &graph) {
  const int INF = INT32_MAX;
  vector<int> min_edge(graph.size(), INF);
  vector<bool> used(graph.size(), false);
  set<pair<int, int>> queue;
  long long ans = 0;
  queue.insert({0, 0});
  min_edge[0] = 0;
  while (!queue.empty()) {
    int curVertex = queue.begin()->second;
    used[curVertex] = true;
    ans += queue.begin()->first;
    queue.erase(queue.begin());
    for (auto neigh : graph[curVertex]) {
      int to = neigh.to, cost = neigh.cost;
      if (cost < min_edge[to] && !used[to]) {  // Добвляем самое минимальное ребро из каждой достижимой
        queue.erase({min_edge[to], to});
        min_edge[to] = cost;
        queue.insert({min_edge[to], to});
      }
    }
  }
  return ans;
}
